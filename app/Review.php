<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $dates = [
        'date_review_done'
    ];
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }
}
