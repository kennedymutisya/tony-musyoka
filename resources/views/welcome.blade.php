<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slider.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('lib/slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('lib/slick/slick-theme.css') }}">
    <link href="{{ asset('css/portfolio.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/icofont.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/icofont.css') }}">
</head>

<body>
<div class="page-wrapper"><!-- Preloader -->
    <div class="preloader">

    </div>
    <header class="main-header header-style-one">
        <!-- Header Upper -->
        <div class="header-upper">
            <div class="inner-container">
                <div class="auto-container clearfix"><!--Info-->
                    <div class="logo-outer">
                        <div class="logo">
                            <a href="#">
                                <img src="{{ asset('logo.png') }}" alt="" title="">
                            </a>
                        </div>
                    </div>

                    <!--Nav Box-->
                    <div class="nav-outer clearfix">
                        <!--Mobile Navigation Toggler For Mobile-->
                        <div class="mobile-nav-toggler default-mobile-nav-toggler"><span
                                class="icon flaticon-menu"></span></div>
                        <nav class="main-menu navbar-expand-md navbar-light">
                            <div class="navbar-header">
                                <!-- Togg le Button -->
                                <button class="navbar-toggler" type="button" data-toggle="collapse"
                                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                        aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="icon flaticon-menu"></span>
                                </button>
                            </div>

                            <div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li><a href="#">Home</a></li>
                                    <li class="dropdown">
                                        <a href="#">About</a>
                                        <ul>
                                            <li><a href="{{ url('about-tony-musyoka-photography') }}">About Us</a></li>
                                            <li><a href="#">Our Team</a></li>
                                            <li><a href="#">Faq's</a></li>
                                            <li><a href="#">Pricing</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Cases</a></li>
                                    <li class="dropdown">
                                        <a href="#">Services</a>
                                        <ul>
                                            <li><a href="#">All Services</a></li>
                                            <li><a href="#">Wedding Photography</a></li>
                                            <li><a href="#">Event Photography</a></li>
                                            <li><a href="#">Special Events Photography</a></li>
                                            <li><a href="#">Family Photography</a></li>
                                            <li><a href="#">Corporate Photography</a></li>
                                            <li><a href="#">Studio Shoots</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->

                        <!-- Outer Box -->
                        <div class="outer-box clearfix">

                            <!-- Main Menu End-->
                            <div class="nav-box">
                                <div class="nav-btn nav-toggler navSidebar-button clearfix">
                                    <span class="icon"></span>
                                    <span class="icon"></span>
                                    <span class="icon"></span>
                                </div>
                            </div>

                            <!-- Search Box Button -->
                            <div class="search-box-btn"><span class="icon flaticon-magnifying-glass-1"></span></div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--End Header Upper-->

        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><span class="icon flaticon-cancel"></span></div>

            <nav class="menu-box">
                <div class="nav-logo"><a href="#"><img src="{{ asset('logo.png') }}" alt="" title=""></a></div>
                <div class="menu-outer">
                    <ul class="navigation clearfix"><!--Keep This Empty / Menu will come through Javascript--></ul>
                </div>
                <!--Social Links-->
                <div class="social-links">
                    <ul class="clearfix">
                        <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                        <li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
                        <li><a href="#"><span class="fab fa-pinterest-p"></span></a></li>
                        <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                        <li><a href="#"><span class="fab fa-youtube"></span></a></li>
                    </ul>
                </div>
            </nav>
        </div><!-- End Mobile Menu -->


    </header>
    <!-- End Main Header -->

    <!-- Main Slider -->
    <section class="main-slider">
        <!-- Pattern Layers -->
        <div class="pattern-layers">
            <div class="layer-one" style="background-image: url(images/main-slider/pattern-1.png)"></div>
            <div class="layer-two" style="background-image: url(images/main-slider/pattern-2.png)"></div>
        </div>
        <!-- Bottom Layers -->
        <div class="bottom-layers">
            <div class="layer-one"></div>
            <div class="layer-two"></div>
        </div>
        <div class="carousel-outer">

            <!-- Banner Carousel -->
            <div class="banner-carousel owl-theme owl-carousel">

                <!-- Slide -->
                <div class="slide-item">
                    <div class="auto-container">
                        <div class="row clearfix">

                            <!-- Content Column -->
                            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                                <div class="inner-column">
                                    <div class="content">
                                        <h2>Your Photography <br> Solution(s)</h2>
                                        <div class="text">We handle photography and event planning so that
                                            you can concentrate on what matters.
                                        </div>
                                        <div class="btns-box">
                                            <a href="#" class="theme-btn btn-style-one"><span
                                                    class="txt">Try Us</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Image Column -->
                            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                                <div class="inner-column">
                                    <div class="image">
                                        <img src="{{ asset('images/main-slider/content-image-1.png') }}" alt=""/>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                s
            </div>

        </div>
    </section>
    <!-- End Banner Section -->

    <!-- Offer Section -->
    <section class="offer-section">
        <div class="side-box"></div>
        <div class="auto-container">
            <div class="sec-title centered">
                <h2>What are we offering you?</h2>
            </div>

            <div class="row clearfix">

                <!-- Offer Block -->
                <div class="offer-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="icon-outer">
                            <div class="icon-box">
                                <span class="icon flaticon-advertising"></span>
                            </div>
                        </div>
                        <h3><a href="#">Wedding Photography</a></h3>
                        <div class="text">Our primary objective is to provide superior quality wedding photography.
                        </div>
                    </div>
                </div>

                <!-- Offer Block -->
                <div class="offer-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="250ms" data-wow-duration="1500ms">
                        <div class="icon-outer">
                            <div class="icon-box">
                                <span class="icon flaticon-loupe"></span>
                            </div>
                        </div>
                        <h3><a href="#">Family Photography</a></h3>
                        <div class="text">We strive to capture natural and enduring portraits for individuals and
                            families, that will be cherished for a lifetime.
                        </div>
                    </div>
                </div>

                <!-- Offer Block -->
                <div class="offer-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="500ms" data-wow-duration="1500ms">
                        <div class="icon-outer">
                            <div class="icon-box">
                                <span class="icon flaticon-design"></span>
                            </div>
                        </div>
                        <h3><a href="#">Special Events Photography</a></h3>
                        <div class="text">We understand the value of your event photography and have experience covering
                            both large events and intimate private events.
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

    <!-- Seo Section -->
    <section class="seo-section">
        <div class="auto-container">
            <div class="row clearfix">

                <!-- Content Column -->
                <div class="content-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <!-- Sec Title -->
                        <div class="sec-title">
                            <div class="sec-icons">
                                <span class="icon-one"></span>
                                <span class="icon-two"></span>
                                <span class="icon-three"></span>
                            </div>
                            <h2>Access your photos from anywhere and from any device</h2>
                        </div>
                        <div class="text">We enable you to access you photographs from anywhere and from any device.
                            You can then select the photos you want for print pay for them in a click of a button, or
                            even save them to your device.
                        </div>
                        <a href="#" class="theme-btn btn-style-two"><span class="txt">Go to your gallery.</span></a>
                    </div>
                </div>

                <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="{{ asset('browser.png') }}" alt=""/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Seo Section -->

    <!-- Services Section -->
    <section class="services-section">
        <div class="auto-container">
            <div class="sec-title light centered">
                <h2>Discover Services we<br> Provide</h2>
            </div>

            <div class="row clearfix">

                <!-- Service Block -->
                <div class="service-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="icon-outer">
                            <div class="icon-box">
                                <span class="icon"><img src="images/icons/service-icon-1.png" alt=""/></span>
                            </div>
                        </div>
                        <h3>Wedding <br>Photography</h3>
                        <div class="text">Our primary objective is to provide superior quality wedding photography.
                        </div>

                        <!--Overlay Box-->
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="content">
                                    <h4><a href="#">Wedding <br>Photography</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Service Block -->
                <div class="service-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="icon-outer">
                            <div class="icon-box">
                                <span class="icon"><img src="images/icons/service-icon-2.png" alt=""/></span>
                            </div>
                        </div>
                        <h3>Special Events <br>Photography</h3>
                        <div class="text">We understand the value of your event photography and have experience covering
                            both large events and intimate private events.
                        </div>

                        <!--Overlay Box-->
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="content">
                                    <h4><a href="#">Special Events <br>Photography</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Service Block -->
                <div class="service-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="icon-outer">
                            <div class="icon-box">
                                <span class="icon"><img src="images/icons/service-icon-3.png" alt=""/></span>
                            </div>
                        </div>
                        <h3>Family <br>Photography</h3>
                        <div class="text">We strive to capture natural and enduring portraits for individuals and
                            families, that will be cherished for a lifetime.
                        </div>

                        <!--Overlay Box-->
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="content">
                                    <h4><a href="#">Family <br>Photography</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Service Block -->
                <div class="service-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="icon-outer">
                            <div class="icon-box">
                                <span class="icon"><img src="images/icons/service-icon-4.png" alt=""/></span>
                            </div>
                        </div>
                        <h3>Corporate <br> Photography</h3>
                        <div class="text">Our corporate photographers work to your brief. Whether you need pictures for
                            marketing material, press, online galleries or simply to remember the best bits
                        </div>

                        <!--Overlay Box-->
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="content">
                                    <h4><a href="#">Corporate <br>Photography</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Service Block -->
                <div class="service-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="icon-outer">
                            <div class="icon-box">
                                <span class="icon"><img src="images/icons/service-icon-5.png" alt=""/></span>
                            </div>
                        </div>
                        <h3>Outdoor Shoots<br>Photography</h3>
                        <div class="text">If you’re looking for that Editorial, yet at the same time it’s simple and
                            natural feeling to your photographs, our team of creatives are ready to shoot at designated
                            outdoor locations and make your photos come alive and those moments saved for you forever.
                        </div>

                        <!--Overlay Box-->
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="content">
                                    <h4><a href="#">Outdoor Shoots<br>Photography</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Service Block -->
                <div class="service-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="icon-outer">
                            <div class="icon-box">
                                <span class="icon"><img src="images/icons/service-icon-6.png" alt=""/></span>
                            </div>
                        </div>
                        <h3>Studio Shoots <br>Photography</h3>
                        <div class="text">We strive to capture natural and enduring portraits for individuals and
                            families, that will be cherished for a lifetime.
                        </div>

                        <!--Overlay Box-->
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="content">
                                    <h4><a href="#">Studio Shoots <br>Photography</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Services Section -->

    <!-- Price Section -->
    <section class="price-section">
        <div class="side-box"></div>

    </section>

    <!-- Fluid Section One -->
    <section class="fluid-section-one">
        <div class="outer-container clearfix">

            <!--Image Column-->
            <div class="image-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms"
                 style="background-image: url(images/resource/video-img.jpg)">
                <div class="inner-column">
                    <div class="image">
                        <img src="images/resource/video-img.jpg" alt="">
                    </div>
                    <a href="https://www.youtube.com/watch?v=SXZXtD60t2g" class="overlay-link lightbox-image">
                        <div class="icon-box">
                            <span class="icon flaticon-play-button"></span>
                        </div>
                    </a>
                </div>
            </div>
            <!--End Image Column-->

            <!--Content Column-->
            <div class="content-column">
                <div class="content-box">
                    <div class="testimonial-carousel owl-carousel owl-theme">

                        <!-- Testimonial Block -->
                        <div class="testimonial-block">
                            <div class="inner-box">
                                <div class="image-outer">
                                    <div class="image">
                                        <img src="images/resource/author-1.jpg" alt=""/>
                                    </div>
                                </div>
                                <div class="text">Lorem ipsum dolor amet constur adipisicing elit sed eiusmtempor incid
                                    dolore magna aliqu. enim minim veniam quis nostrud exercittion.ullamco laboris nisi
                                    ut aliquip excepteur sint oc caecat cuida tat nonproident sunt in culpa qui officia
                                    deserunt mollit anim est laborum sed ut perspiciatis.
                                </div>
                                <h3>Alice Kelly</h3>
                                <div class="author">Happy Client</div>
                            </div>
                        </div>

                        <!-- Testimonial Block -->
                        <div class="testimonial-block">
                            <div class="inner-box">
                                <div class="image-outer">
                                    <div class="image">
                                        <img src="images/resource/author-1.jpg" alt=""/>
                                    </div>
                                </div>
                                <div class="text">Lorem ipsum dolor amet constur adipisicing elit sed eiusmtempor incid
                                    dolore magna aliqu. enim minim veniam quis nostrud exercittion.ullamco laboris nisi
                                    ut aliquip excepteur sint oc caecat cuida tat nonproident sunt in culpa qui officia
                                    deserunt mollit anim est laborum sed ut perspiciatis.
                                </div>
                                <h3>Alice Kelly</h3>
                                <div class="author">Happy Client</div>
                            </div>
                        </div>

                        <!-- Testimonial Block -->
                        <div class="testimonial-block">
                            <div class="inner-box">
                                <div class="image-outer">
                                    <div class="image">
                                        <img src="images/resource/author-1.jpg" alt=""/>
                                    </div>
                                </div>
                                <div class="text">Lorem ipsum dolor amet constur adipisicing elit sed eiusmtempor incid
                                    dolore magna aliqu. enim minim veniam quis nostrud exercittion.ullamco laboris nisi
                                    ut aliquip excepteur sint oc caecat cuida tat nonproident sunt in culpa qui officia
                                    deserunt mollit anim est laborum sed ut perspiciatis.
                                </div>
                                <h3>Alice Kelly</h3>
                                <div class="author">Happy Client</div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End Fluid Section One -->

    <!-- Website Section -->
    <section class="website-section">
        <div class="auto-container">
            <!-- Sec Title -->
            <div class="sec-title light centered">
                <h2>Seen Enough? <br> Fill in your details.</h2>
            </div>

            <!-- Website Form-->
            <div class="website-form">
                <form method="post" action="http://t.commonsupport.com/amatic/contact.html">
                    <div class="form-group clearfix">
                        <input type="text" name="website" value="" placeholder="Your Phone Number" required>
                        <input type="email" name="email" value="" placeholder="Your Email" required>
                    </div>
                    <button type="submit" class="theme-btn btn-style-three">Submit</button>
                </form>
            </div>

        </div>
    </section>
    <!-- End Website Section -->

    <!-- News Section -->
    <section class="news-section">
        <div class="auto-container">
            <div class="sec-title centered">
                <h2>Latest News</h2>
            </div>

            <div class="row clearfix">

                <!-- Column -->
                <div class="column col-lg-8 col-md-12 col-sm-12">
                    <div class="row clearfix">

                        <!-- News Block -->
                        <div class="news-block col-lg-6 col-md-6 col-sm-12">
                            <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <div class="image">
                                    <a href="#"><img src="images/resource/news-1.jpg" alt=""/></a>
                                </div>
                                <div class="lower-content">
                                    <div class="title">Article</div>
                                    <ul class="post-info">
                                        <li>by <span class="dark">admin</span></li>
                                        <li>on <span class="theme_color">2 Sep, 2018</span></li>
                                    </ul>
                                    <h3><a href="#">Dishes That Amplify Your Feelings</a></h3>
                                    <div class="text">Excepteur sint ocacat cupidatat non proi dent sunt in culpa qui
                                        officia deserunt. mollit anim id est laborum. sed ut pers piciatis unde omnis
                                        iste natus error. sit voluptatem.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- News Block -->
                        <div class="news-block col-lg-6 col-md-6 col-sm-12">
                            <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <div class="image">
                                    <a href="#"><img src="images/resource/news-2.jpg" alt=""/></a>
                                </div>
                                <div class="lower-content">
                                    <div class="title pink">Article</div>
                                    <ul class="post-info">
                                        <li>by <span class="dark">admin</span></li>
                                        <li>on <span class="theme_color">2 Sep, 2018</span></li>
                                    </ul>
                                    <h3><a href="#">Dishes That Amplify Your Feelings</a></h3>
                                    <div class="text">Excepteur sint ocacat cupidatat non proi dent sunt in culpa qui
                                        officia deserunt. mollit anim id est laborum. sed ut pers piciatis unde omnis
                                        iste natus error. sit voluptatem.
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <!-- Column -->
                <div class="sidebar-column col-lg-4 col-md-12 col-sm-12">
                    <div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">

                        <!-- News Block Two -->
                        <div class="news-block-two">
                            <div class="inner-box">
                                <div class="title green">Article</div>
                                <ul class="post-info">
                                    <li>by <span class="dark">admin</span></li>
                                    <li>on <span class="theme_color">2 Sep, 2018</span></li>
                                </ul>
                                <h3><a href="#">Dishes That Amplify Your Feelings</a></h3>
                            </div>
                        </div>

                        <!-- News Block Two -->
                        <div class="news-block-two">
                            <div class="inner-box">
                                <div class="title blue">Article</div>
                                <ul class="post-info">
                                    <li>by <span class="dark">admin</span></li>
                                    <li>on <span class="theme_color">2 Sep, 2018</span></li>
                                </ul>
                                <h3><a href="#">Dishes That Amplify Your Feelings</a></h3>
                            </div>
                        </div>

                        <!-- News Block Two -->
                        <div class="news-block-two">
                            <div class="inner-box">
                                <div class="title orrange">Article</div>
                                <ul class="post-info">
                                    <li>by <span class="dark">admin</span></li>
                                    <li>on <span class="theme_color">2 Sep, 2018</span></li>
                                </ul>
                                <h3><a href="#">Dishes That Amplify Your Feelings</a></h3>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- End News Section -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="auto-container">
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">

                    <!--Footer Column-->
                    <div class="footer-column col-lg-4 col-md-6 col-sm-12">
                        <div class="footer-widget logo-widget">
                            <div class="logo">
                                <a href="#"><img src="{{ asset('logo.png') }}" alt=""/></a>
                            </div>
                            <div class="text">TonyMusyoka Photography is a photo & video studio that is based in Kenya,
                                but
                                available for travel all over the world. We are a group of professional photographers,
                                cinematographers & editors who pride themselves in providing quality service at a budget
                                price. You will find our prices & quality hard to beat in today’s growing market.
                            </div>
                            <ul class="social-icons">
                                <li class="facebook"><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                                <li class="twitter"><a href="#"><span class="fab fa-twitter"></span></a></li>
                                <li class="vimeo"><a href="#"><span class="fab fa-vimeo-v"></span></a></li>
                                <li class="linkedin"><a href="#"><span class="fab fa-linkedin-in"></span></a></li>
                            </ul>
                        </div>
                    </div>

                    <!--Footer Column-->
                    <div class="footer-column col-lg-3 col-md-6 col-sm-12">
                        <div class="footer-widget links-widget">
                            <h2>Quick Link</h2>
                            <div class="widget-content">
                                <ul class="list">
                                    <li><a href="#">Company History</a></li>
                                    <li><a href="{{ url('about-tony-musyoka-photography') }}">About us</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!--Footer Column-->
                    <div class="footer-column col-lg-5 col-md-12 col-sm-12">
                        <div class="footer-widget newsletter-widget">
                            <h2>Newsletter</h2>
                            <div class="text">You can subscribe our newsletter and get updates about our offers </div>
                            <!-- Newsletter Form -->
                            <div class="newsletter-form">
                                <form method="post" action="http://t.commonsupport.com/amatic/contact.html">
                                    <div class="form-group">
                                        <input type="email" name="email" value="" placeholder="Your email" required>
                                        <button type="submit" class="theme-btn btn-style-four"><span class="txt">Subscribe</span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Footer Bottom -->
            <div class="footer-bottom">
                <div class="clearfix">
                    <div class="pull-left">
                        <div class="copyright">TonyMusyoka Photography &copy; {{date('Y')}} All Right Reserved</div>
                    </div>
                    <div class="pull-right">
                        <ul class="footer-list">
                            <li><a href="#">Terms of Service</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </footer>
    <!-- End Main Footer -->

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-up"></span></div>

<!--Search Popup-->
<div id="search-popup" class="search-popup">
    <div class="close-search theme-btn"><span class="flaticon-cancel"></span></div>
    <div class="popup-inner">
        <div class="overlay-layer"></div>
        <div class="search-form">
            <form method="post" action="http://t.commonsupport.com/amatic/index.html">
                <div class="form-group">
                    <fieldset>
                        <input type="search" class="form-control" name="search-input" value="" placeholder="Search Here"
                               required>
                        <input type="submit" value="Search Now!" class="theme-btn">
                    </fieldset>
                </div>
            </form>

        </div>

    </div>
</div>


<!-- sidebar cart item -->
<div class="xs-sidebar-group info-group info-sidebar">
    <div class="xs-overlay xs-bg-black"></div>
    <div class="xs-sidebar-widget">
        <div class="sidebar-widget-container">
            <div class="widget-heading">
                <a href="#" class="close-side-widget">
                    X
                </a>
            </div>
            <div class="sidebar-textwidget">

                <!-- Sidebar Info Content -->
                <div class="sidebar-info-contents">
                    <div class="content-inner">
                        <div class="logo">
                            <a href="#"><img src="{{ asset('logo.png') }}" alt=""/></a>
                        </div>
                        <div class="content-box">
                            <h2>About Us</h2>
                            <p class="text">TonyMusyoka Photography is a photo & video studio that is based in Kenya, but available for travel all over the world. We are a group of professional photographers, cinematographers & editors who pride themselves in providing quality service at a budget price. You will find our prices & quality hard to beat in today’s growing market.
                            </p>
                            <a href="#" class="theme-btn btn-style-one"><span class="txt">Consultation</span></a>
                        </div>
                        <div class="contact-info">
                            <h2>Contact Info</h2>
                            <ul class="list-style-one">
                                <li><span class="icon flaticon-map"></span>Eldorect, Kenya</li>
                                <li><span class="icon flaticon-telephone"></span><a
                                        href="tel:0718593530">0718 593 530</a></li>
                                <li><span class="icon flaticon-message-1"></span><a href="mailto:info@tonymusyokaphotagraphy.com">info@tonymusyokaphotagraphy.com</a>
                                </li>
                                <li><span class="icon flaticon-timetable"></span>Week Days: 09.00 to 18.00 Sunday:
                                    Closed
                                </li>
                            </ul>
                        </div>
                        <!-- Social Box -->
                        <ul class="social-box">
                            <li class="facebook"><a href="#" class="fab fa-facebook-f"></a></li>
                            <li class="twitter"><a href="#" class="fab fa-twitter"></a></li>
                            <li class="linkedin"><a href="#" class="fab fa-linkedin-in"></a></li>
                            <li class="instagram"><a href="#" class="fab fa-instagram"></a></li>
                            <li class="youtube"><a href="#" class="fab fa-youtube"></a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- END sidebar widget item -->


<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/owl.js"></script>
<script src="js/nav-tool.js"></script>
<script src="js/scrollbar.js"></script>
<script src="js/wow.js"></script>
<script src="js/appear.js"></script>
<script src="js/script.js"></script>

</body>
</html>
